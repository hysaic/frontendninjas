$('#clock').fitText(1.3);

function update() {
    $('#clock').html(moment().format('D. MMMM YYYY H:mm:ss'));
}

setInterval(update, 1000);

function DayDiff(CurrentDate) {
    var TYear = CurrentDate.getFullYear();
    var TDay = new Date("January, 01, 2016");
    TDay.getFullYear(TYear);
    var DayCount = (TDay - CurrentDate) / (1000 * 60 * 60 * 24);
    DayCount = Math.round(DayCount);
    return (DayCount);
}