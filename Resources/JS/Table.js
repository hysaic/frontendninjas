(function () {
    var app = angular.module('app', ['smart-table']);

    var MainController = function ($scope, $http) {

        var onRequestComplete = function (response) {
            $scope.api_status = response.data;
            if ($scope.api_status.resources.core.remaining===0) {
                $scope.timeToReset = $scope.api_status.resources.core.reset;
            }
        }
        var onErr = function(reason){
            $scope.api_status = reason.data;
        }
        $http.get("https://api.github.com/rate_limit")
            .then(onRequestComplete,onErr);
    };
    //---------------------------------------------------------
    var UsersController = function ($scope, $http) {
        var onUserComplete = function (response) {
            $scope.users = response.data;
        }
        var onError = function (reason) {
            $scope.error = "Couldn´t find users";
        }
        $http.get("https://api.github.com/users")
            .then(onUserComplete, onError);
        $scope.displayedCollection = [].concat($scope.users);
        $scope.itemsByPage = 5;
    };
    //---------------------------------------------------------
    var UserController = function ($scope, $http) {

        var onUserComplete = function (response) {

            $scope.user = response.data;
            $http.get($scope.user.repos_url).then(getRepos);
        }
        var getRepos = function (response) {
            $scope.user.repos = response.data.length;
        }
        var onError = function (reason) {
            $scope.error = "Couldn´t find user";
            $scope.user = null;
        }
        $scope.search = function (username) {
            $scope.error = "";
            $http.get("https://api.github.com/users/" + username)
            .then(onUserComplete, onError);
        }

    }
    //---------------------------------------------------------
    app.controller('UsersController', UsersController);
    app.controller('UserController', UserController);
    app.controller('MainController', MainController);
})();