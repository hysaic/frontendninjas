controllers.WeatherController = function ($scope, $http) {
	$scope.init = function () {
var url = "https://api.forecast.io/forecast/cd49a800bcb125f0dbba4aa3c33963ae/"
	var lon = "18.068581";
	var lat = "59.329323";
	var stad = "Stockholm";
	var coordUrl = "https://nominatim.openstreetmap.org/search"
	var locSplit;
	GetWeather();
	// weatherTesting();
	
		
	$('#subCity').click(function(){		
		
		var reqStad = $('#inpCity').val();
		var reqUrl = coordUrl + "?q=" + reqStad + "&format=json"
			
		$.ajax({	
			type: "GET", 
			dataType: "json",
  			url: reqUrl,			  	
			success: function(data){			  
			    var retData = data[0];			 		   		  
				lon = retData.lon;
				lat = retData.lat;
				locSplit = retData.display_name.split(',');
				stad = locSplit[0];								
				GetWeather();
			},
			error:function(jqXHR, textStatus, errorThrown){
				alert("could not get city coordinates"+", "+ jqXHR.status+ ", "+ errorThrown);
				GetWeather();
			}
	});
	
	});	
	
	
	function GetWeather(){
		var reqUrl = url + lat + "," + lon;
		// alert(reqUrl);
			$.ajax({
	
		dataType: "jsonp",
  		url: reqUrl, 		  			  	
		type: "GET", 		  
  		success: function(data){
			  var retData = data;
			  var weather = retData.currently.icon.toLowerCase();
			  var degCelsius = (parseFloat(retData.currently.temperature) -32) * 5/9;
			  var infoText = "";			  		  
				infoText += "The weather in " + stad + " is " + retData.currently.summary.toLowerCase()
				infoText += " and the temperature is " + degCelsius.toFixed(2) + "°C";
				GetIcon(weather, $('.weatherInfoWrapper').find('img'));
				
				$(".weatherSpan h2").text(retData.currently.summary.toLowerCase());
				$("#currentCity").find('h2').text("Current weather in " + stad);		
				$("#weeklyCity").find('h2').text("Weather rest of this week in " + stad);
				
				$("#currentTemp").find('h2').text(degCelsius.toFixed(2)+ "°C");
				// appendWeeklyData(data);
								
			},
		error:function(jqXHR, textStatus, errorThrown){
				alert(jqXHR.status);
			}
	});
	}

function GetIcon(weather, img){
	
	
	var sourceDir = 'Resources/img/weatherIcons/';
	switch (weather) {
		case "partly-cloudy-day":
			img.attr('src',sourceDir + "partly-cloudy-day.png" );			
			break;
		case "partly-cloudy-night":
			img.attr('src',sourceDir + "partly-cloudy-night.png" );
			break;
		case "cloudy":
			img.attr('src',sourceDir + "cloudy.png" );
			break;
		case "snow":
			img.attr('src',sourceDir + "snow.png" );
			break;
		case "rain":
			img.attr('src',sourceDir + "rain.png" );
			break;
		case "clear-day":
			img.attr('src',sourceDir + "clear-day.png" );			
			break;
		case "clear-night":
			img.attr('src',sourceDir + "clear-night.png" );
			break;
		case "sleet":
			img.attr('src',sourceDir + "sleet.png" );
			break;
		case "fog":
			img.attr('src',sourceDir + "clear-day.png" );			
			break;	
		case "wind":
			img.attr('src',sourceDir + "cloudy.png" );				
			break;		
		
		default:
		break;
		
		
	}
}
	function weatherTesting()
	{
			$('.miniWeatherWrapper').find(".weatherSpan h2").text("Mostly cloudy");			
				$('.miniWeatherWrapper').find(".tempSpan h2").text("14.24°C");
				GetIcon("partly-cloudy-night", $('.miniWeatherWrapper').find('img'));
				var currentTIme = new Date();
				
	}
	function appendWeeklyData(data)
	{
		
		$.each(data.daily.data, function(){
			
			var degCelsiusMin = (parseFloat(this.temperatureMin) -32) * 5/9;
			var degCelsiusMax = (parseFloat(this.temperatureMax) -32) * 5/9;
			$('.miniWeatherWrapper').find('.row').append('<div class="miniWeatherWrapper col-lg-2 col-md-2 col-sm-3 col-xs-6">').append
			('<div class="textWrapper  col-xs-12 upper" >').append('<span class="weatherSpan"><h2 class="mini">'+ this.summary +'</h2></span>').append('</div> </div>');
	
		});
	}

	};
}