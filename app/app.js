// create module and insert routing:
var frontendNinjas = angular.module('frontendNinjas', ['ui.router', 'smart-table', 'ui.bootstrap']);
//var frontendNinjas = angular.module('frontendNinjas', ['ngRoute']);

// variable that holds all controllers:
var controllers = {};

var registeredUsers = [{"username": "test", "email": "test@test.com", "password": "test123"}];

//-------- factories --------//
frontendNinjas.factory('ninjaFactory', function () {
	var factory = {};
	factory.Test = function () {
		return "this is a test";
	};

	return factory;
});

frontendNinjas.factory("UsersApi", function ($q, $http, $rootScope) {
    function _login (email, password) {
        var d = $q.defer();
        var user = $.grep(registeredUsers, function(e){ return e.email == email; });
        
        console.log("user:", user[0]);

        if(password === user[0].password){
            d.resolve(user[0]);
        }
        
        return d.promise
    }

    function _register (username, email, password) {
        var d = $q.defer();
        var result = $.grep(registeredUsers, function(e){ return e.username == username; });
        
        if(result.length == 0){
            var user = {
                username: username,
                email: email,
                password: password
            };
            d.resolve(user);
        }
        return d.promise
    }

    return {
        login: _login,
        register: _register
    };
});
//-------- end of: factories --------//

//-------- controllers --------//
controllers.ninjaController = function ($scope, ninjaFactory) {
	$scope.test = "test";
	function init() {
		$scope.test = ninjaFactory.Test();
	}
	init();
};

//github smart table controlers
controllers.MainController = function ($scope, $http) {

	var onRequestComplete = function (response) {
		$scope.api_status = response.data;
		if ($scope.api_status.resources.core.remaining === 0) {
			$scope.timeToReset = api_status.resources.core.reset;
		}
	}
	var onErr = function (reason) {
		$scope.api_status = reason.data;
	}
	$http.get("https://api.github.com/rate_limit")
		.then(onRequestComplete, onErr);
};

controllers.UsersController = function ($scope, $http) {
	var onUserComplete = function (response) {
		$scope.users = response.data;
	}
	var onError = function (reason) {
		$scope.error = "Couldn´t find users";
	}
	$http.get("https://api.github.com/users")
		.then(onUserComplete, onError);
	$scope.displayedCollection = [].concat($scope.users);
	$scope.itemsByPage = 5;
};

controllers.UserController = function ($scope, $http) {

	var onUserComplete = function (response) {

		$scope.user = response.data;
		$http.get($scope.user.repos_url).then(getRepos);
	}
	var getRepos = function (response) {
		$scope.user.repos = response.data.length;
	}
	var onError = function (reason) {
		$scope.error = "Couldn´t find user";
		$scope.user = null;
	}
	$scope.search = function (username) {
		$scope.error = "";
		$http.get("https://api.github.com/users/" + username)
            .then(onUserComplete, onError);
	}

}
//end of github smart tables controllers

//mixit controller
controllers.MixitController = function ($scope, $http) {
    $scope.init = function () {
        var $elements = $();

        $http.get("https://api.myjson.com/bins/4bg7q").success(function (data) {
            $scope.animals = data.animals;
            var objects
            $.each($scope.animals, function (i, val) {
                var $newObject = '<div class="mix ' + val.type + '" data-myorder="' + i + '"> <img src="' + val.imageurl + '" alt="http://4.bp.blogspot.com/_hxmU8cidV-Q/TUiIuZr8EfI/AAAAAAAACSU/SEmZSmEagYo/s1600/404%2BFILE%2BNOT%2BFOUND.png" /> </div>';
                $elements = $elements.add($newObject);
            });
            var $gapObject = '<div class="gap"></div>';
            $elements = $elements.add($gapObject);
            $elements = $elements.add($gapObject);
            $('#Container').append($elements);
            $('#Container').mixItUp();
        });
    };

    $scope.$on('$locationChangeStart', function( event ) {
        $('#Container').mixItUp('destroy');
    });
}
//end of mixit controller

frontendNinjas.controller('RegisterUserCtrl', function($scope, UsersApi){
    $scope.submit = function (username, email, password) {

        UsersApi.register(username, email, password).then(function (user) {
            registeredUsers.push(user);
            console.log("registeredUsers: ", registeredUsers);
        });
    };
});

//LoginModalCtrl
frontendNinjas.controller('LoginModalCtrl', function ($scope, UsersApi) {
    $scope.users = registeredUsers;
    this.cancel = $scope.$dismiss;
    this.submit = function (email, password) {
        UsersApi.login(email, password).then(function (user) {
            $scope.users = registeredUsers;
            $scope.$close(user);
        });
    };
});
//end of LoginModalCtrl

//weather controller in own file

//-------- end of: controllers --------//

//make all controllers known to the module:
frontendNinjas.controller(controllers);

//-------- route config --------//
frontendNinjas.config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
    $httpProvider.interceptors.push(function($timeout, $q, $injector){
       var loginModal, $http, $state;
       
       $timeout(function(){
           loginModal = $injector.get('loginModal');
           $http = $injector.get('$http');
           $state = $injector.get('$state');
       });

       return {
         responseError: function(rejection){
             if(rejection.status !== 401){
                 return rejection;
             }

             var deferred = $q.defer();

             loginModal()
             .then(function(){
                 deferred.resolve($http(rejection.config));
             })
             .catch(function(){
                 $state.go('/');
                 deferred.reject(rejection);
             });

             return deferred.promise;
         }  
       };
    });

    $urlRouterProvider.otherwise("/");

    $stateProvider
		.state('/',
			{
			    url: "/",
			    controller: 'ninjaController',
			    templateUrl: 'partials/frontpage.html',
                data: {
                    requireLogin: false
                }
			})
		.state('/01',
			{
                url: "/01",
			    controller: 'ninjaController',
			    templateUrl: 'partials/partial01.html',
                data: {
                    requireLogin: false
                }
			})
		.state('/02',
			{
                url: "/02",
			    controller: 'ninjaController',
			    templateUrl: 'partials/partial02.html',
                data: {
                    requireLogin: false
                }
			})
		.state('/klocka',
			{
                url: "/klocka",
			    controller: 'ninjaController',
			    templateUrl: 'app.klocka.html',
                data: {
                    requireLogin: false
                }
			})
		.state('/weather',
			{
                url: "/weather",
			    controller: 'WeatherController',
			    templateUrl: 'app.weather.html',
                data: {
                    requireLogin: false
                }
			})
		.state('/mixit',
			{
                url: "/mixit",
			    templateUrl: 'app.mixit.html',
			    controller: 'MixitController',
                data: {
                    requireLogin: false
                }
			})
		.state('/github',
			{
                url: "/github",
			    controller: 'ninjaController',
			    templateUrl: 'github.html',
                data: {
                    requireLogin: false
                }
			}).
        state('/secret',
		{
            url: "/secret",
			controller: 'LoginModalCtrl',
			templateUrl: 'secret.html',
            data: {
                requireLogin: true
            }
		})
        .state('/login',
			{
                url: "/login",
			    controller: 'LoginModalCtrl',
			    templateUrl: 'loginModalTemplate.html',
                data: {
                    requireLogin: false
                }
			})
        .state('/register',
		{
            url: "/register",
			controller: 'RegisterUserCtrl',
			templateUrl: 'registeruser.html',
            data: {
                requireLogin: false
            }
		});
});
//-------- end of: route config --------//

frontendNinjas.run(function ($rootScope, $state, loginModal) {
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
        var requireLogin = toState.data.requireLogin;

        if (requireLogin && typeof $rootScope.currentUser === 'undefined') {
            event.preventDefault();

            loginModal()
            .then(function () {
                return $state.go(toState.name, toParams);
            })
            .catch(function(){
                return $state.go('/register');
            });
        }
    });
});

frontendNinjas.service('loginModal', function ($uibModal, $rootScope) {
    function assignCurrentUser(user) {
        $rootScope.currentUser = user;
        return user;
    }

    return function () {
        var instance = $uibModal.open({
            templateUrl: 'loginModalTemplate.html',
            controller: 'LoginModalCtrl',
            controllerAs: 'LoginModalCtrl'
        })

        return instance.result.then(assignCurrentUser);
    };
});