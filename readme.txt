FRONTEND NINJAS

DataTable {
Ansvarig: Fredrik
Bibliotek: AngularJS, Bootstrap, SmartTable.JS

Två API anrop görs till github.com med AngularJS när sidan laddas.
Det ena kollar om "rate_limit" ===0 och isådant fall använder jag mig att den Unix tidskod som returnerats 
och startar en countdown i vyn till när nästa batch av anrop kommer att tilldelas.
Detta görs med Angulars egna "timer" directive. 

Det andra anropet sker i en separat controller och efter att man verifierat att responsdatat är successful så binds det till controllerns scope objekt.
För att array datat ska kunna bindas till Smart-Table så skapas en separat kopia för sortering, paginering och sökning.

I vyn så binds datat med Angulars model-binding directive ( {{...}} ) samt loopas igenom med "ng-repeat".

Funktionen att söka på enskild användare, även den i en separat controller, liknar ovan, med skillnaden att vyn inte använder sig av SmartTable 
och därför inte behöver en kopia av datat. Däremot behövs ett "ng-model('username')" directive för att flytta input data från vyn 
till controllern och användas i "$scope.search" metoden. Här görs även ett ytterligare anrop för att komma åt antal repositories som användaren har kopplat till sitt konto.

Grundläggande Bootstrap funktionalitet används tillsammans med Angulars "ng-class" för att, med logik, tilldela värde i table datat.
"{'glyphicon glyphicon-lock':!user.site_admin}" 
  
}

Weather {
Ansvarig: Robin

API:s: Forecast.io, openstreetmap.org
jQuery
Html
Css

vid inmatning av plats så anropas openstreetmaps api för att få ut koordinaterna som sedan i sin tur används för att anropa
forecast.io och därifrån få ut väsentlig väderinformation som sedan visas upp tillsammans med rätt ikon för vädret.

}

Clock{
Ansvarig: Christopher
Bibliotek: HTML/CSS , Jquery, JqueryFitText, Bootstrap, Moment.JS, FlipClock.Js

Klockan är gjord med simpel HTML/CSS där responsiviteten utgörs av bootstrap.
Själva funktionen som hämtar tiden är gjord med Jquery med Moment.js som plugin.
Moment.Js är ett plugin där man kan hämta tid i alla olika format.

Timern som räknar ned till nyår är gjord med Flipclock.Js som är ett plugin som bygger på Jquery.
Utöver timern så finns det andra funktioner flipclock fyller.

}

MixItUp/LogIn {
Ansvarig: Marcus O.
    MixItUp {
        Ett anrop görs mot en extern källa för att hämta en lista med objekt som består av en bild-url samt en sträng som definierar typ av djur (katt eller hund).
        Ett div element populeras med nya element skapade utav datan från anropet.
        MixItUp kallas på div elementet och därmed sorteras elementen baserat på de olika filter som i förväg har definierats med hjälp av knappar som nyttjar angular-attributen filter och sort.
    },
    LogIn {
        Registering och inloggning av användare sker helt i frontend med hjälp av angular. Vi använder oss av en routing plugin som heter ui-router som tillåter oss att lägga till egna attribut på en route.
        Detta använder vi för att säga att vissa routes kräver att man är inloggad.
        Vi har en global variabel där vi lagrar registrerade användare som objekt i en array.
        Vi valde att inte göra en "riktig" backend lösning med repositories och tillhörande api eftersom vi ansåg att detta skulle ta för lång tid att implementera samt att vi ansåg att det inte var relevant för den här kursen.
        Den enda skillnaden frontend-mässigt är att vi hämtar data från en variabel i scriptet istället för att hämta data via ett api-anrop.
        När man försöker logga in så kallas en tjänst som letar igenom datan för att se om det finns en användare med den email-adress man matat in och om det lyckas så jämför den den funna användarens lösenord med det som matats in.
        Sedan sätts en variabel i angular's $rootscope till den aktiva användaren och det är när den variabeln är satt till en faktisk användare som man kommer åt de låsta sidorna.
        När man registrerar en ny användare så kallas en annan tjänst som kontrollerar att det inte redan finns en användare med samma användarnamn och om det inte finns så skapas en ny användare som matas in i listan med registrerade användare.
    }
}

Git/SPA-Routing/LogIn {
Ansvarig: Johann
    Bibiliotek: AngularJS med ui-router, Bootstrap
    Git {
        Skapade ett git repository på Github, och använde SourceTree / VS Code som interface för versionshanteringen.
        Projektet är en mappstruktur med filer, utan solution. Fördelen med detta är att få ett lättviktigt projekt som är cross-platform.
        Vi använde olika editorer efter smak. Lokal server hosting med MAMP. 
    },
    Routing {
        Sidan är SPA där partial views routas och visas med hjälp av AngularJS och ui-router.
        Responsivitet vid hjälp av bootstrap klasser.
        Deluppgifterna utvecklades var för sig och lades sen in som partial views i angular-appen. Det gick rätt smidigt men krävde an del modifikationer av html, css och js för att få allting att
        sitta ihop på ett bra sätt.
        OBS en del av html filerna som ligger kvar i projektet används inte i appen utan är tidigare versioner. Vilka som används kan man se i routingen i app.js. En anledning till detta är att jag ibland
        fick jobba med integreringen av delsidor samtidigt som dom var under uteckling. Genom att jobba i olika filer undviker man konflikter. 
    },
    LogIn {
        Samarbete med Marcus O, se ovan
    }
}

